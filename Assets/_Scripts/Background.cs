﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

    public bool isPlayerOnLadder = false; 
    public float speed = 0.5f;
    public GameObject staticBackground;
    public GameObject player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        // animate cloud loop
        UpdateCloudLoop();

        // move static background and cloud loop
       // MoveBackground();
       
	}

    void UpdateCloudLoop() {
        if (isPlayerOnLadder)
        {
            Vector2 offset = new Vector2(0, Time.time * speed);
            GetComponent<Renderer>().material.mainTextureOffset = offset;
        }
        else
        {
            Vector2 oldOffset = GetComponent<Renderer>().material.mainTextureOffset;
            Vector2 offset = new Vector2(0, oldOffset.y);
            GetComponent<Renderer>().material.mainTextureOffset = offset;
        }
    }

}
