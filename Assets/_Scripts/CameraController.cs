﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public PlayerController player;

    private Vector3 lastPlayerPosition;
    private float distanceToMove;

	// Use this for initialization
	void Start () {
        this.player = FindObjectOfType<PlayerController>();
        lastPlayerPosition = this.player.transform.position;
    }

    // Update is called once per frame
    void Update () {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + distanceToMove, this.transform.position.z);
        distanceToMove = this.player.transform.position.y  - this.lastPlayerPosition.y;
        lastPlayerPosition = this.player.transform.position;
	}

}
