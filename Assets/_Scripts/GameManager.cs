﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{

    public LadderFactory ladderFactory;
    public PlayerController thePlayer;
    public LoseCollider loseCollider;
    private ScoreManager scoreManager;

    // Use this for initialization
    void Start()
    {
        scoreManager = FindObjectOfType<ScoreManager>();

    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log("[Game] --------------------- Update()");

    }

    public void RestartGame()
    {
        // use a co-routine, runs independent of our state
        // allow time delay to be added
        StartCoroutine("RestartGameCo");
    }

    public IEnumerator RestartGameCo()
    {
        scoreManager.isScoreIncreasing = false;
        // make the player invisible
        thePlayer.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);

        thePlayer.Reset();
        ladderFactory.Reset();
        loseCollider.Reset();
        scoreManager.Reset();
        thePlayer.gameObject.SetActive(true);
    }
}
