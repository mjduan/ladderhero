﻿using UnityEngine;
using System.Collections;

public class LadderBlock : MonoBehaviour
{
    public Collider2D myCollider;
    public Rigidbody2D myRigidBody;
    public LayerMask playerLayer;

    // Use this for initialization
    void Start()
    {
        myCollider = GetComponent<Collider2D>();
        myRigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
