﻿using UnityEngine;
using System.Collections;

// this script is attached to each instance of ladder
public class LadderDestroyer : MonoBehaviour
{
    public GameObject ladderDestroyPoint;

    // Use this for initialization
    void Start()
    {
        // GameObject.Find will find any object with the given name
        ladderDestroyPoint = GameObject.Find("LadderDestroyPoint");
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.y < ladderDestroyPoint.transform.position.y)
        {
            //Destroy(gameObject);
            gameObject.SetActive(false);
        }
    }
}
