﻿using UnityEngine;
using System.Collections;

public class LadderFactory : MonoBehaviour
{
    //public GameObject[] theLadders; // to be generated ahead of game scene
    public Transform generationPoint;
    public float distanceBetweenLadders;
    //public float ladderHorizontalDistance;

    public ObjectPooler ladderPool;

    public float[] ladderXPositions;

    public Transform leftBoundPoint;
    public Transform rightBoundPoint;
    public float maxLadderGap; // maximum distance between spawned ladders
    public float minLadderGap; // min        "        "       "      "

    private float defaultLadderX;
    private float leftBoundX;
    private float rightBoundX;
    private float ladderLength; // so we dont spawn overlapping ladders
    private float newLadderX;
    private Vector3 origPosition;

    // Use this for initialization
    void Start()
    {
        //Debug.Log("[LadderFactory] theLadder.size: " + theLadder.GetComponent<BoxCollider2D>().size);
        //ladderLength = theLadders[1].GetComponent<BoxCollider2D>().size.y;
        defaultLadderX = this.transform.position.x;
        leftBoundX = leftBoundPoint.position.x;
        rightBoundX = rightBoundPoint.position.x;
        origPosition = this.gameObject.transform.position;
        this.SpawnLadder();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("--------------- [LadderFactory] position: " + this.transform.position);
        //Debug.Log("--------------- [LadderFactory] generationPoint: " + this.generationPoint.position);
        this.SpawnLadder();
    }

    private void CalcNewLadderX()
    {
        float newXGap = Random.Range(maxLadderGap, -maxLadderGap);
        if (Mathf.Abs(newXGap) < minLadderGap)
        {
            float sign = Mathf.Sign(newXGap);
            newXGap = sign * minLadderGap;
        }

        this.newLadderX = this.transform.position.x + newXGap;


        if (this.newLadderX <= this.leftBoundX || this.newLadderX >= this.rightBoundX)
        {
            this.newLadderX += -2 * newXGap;
        }
    }

    private void SpawnLadder()
    {
        if (this.transform.position.y < this.generationPoint.position.y)
        {
            GameObject newLadder = this.ladderPool.getPoolObject();
            this.CalcNewLadderX();

            //Debug.Log("-------------------------------------------------------[LadderFactory] SPAWNING LADDER");
            this.transform.position = new Vector3(
                //this.transform.position.x,
                this.newLadderX,
                this.transform.position.y + this.distanceBetweenLadders + this.ladderLength,
                this.transform.position.z
                );
            //Debug.Log("--------------- this position: " + this.transform.position);

            newLadder.transform.position = this.transform.position;
            newLadder.transform.rotation = this.transform.rotation;
            newLadder.SetActive(true);
            this.ladderLength = newLadder.GetComponent<BoxCollider2D>().size.y;
            //Instantiate(this.theLadder, this.transform.position, this.transform.rotation);
        }
    }

    public void Reset()
    {
        this.ladderPool.Reset();
        this.gameObject.transform.position = this.origPosition;
    }
}