﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

    public void LoadLevel(string name)
    {
        Debug.Log("[LevelManager] Level load requested for: " + name);
        Application.LoadLevel(name);
    }

    public void Quit(string name)
    {
        Debug.Log("[LevelManager] Quit () ");
        Application.Quit();
    }

}
