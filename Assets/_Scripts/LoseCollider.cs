﻿using UnityEngine;
using System.Collections;

public class LoseCollider : MonoBehaviour {

    public PlayerController player;
    public Background cloudBackground;
    public LevelManager levelManager;
    public GameManager gameManager;

    //private Vector3 lastPlayerPosition;
    private Vector3 lastBackgroundPosition;
    private Vector3 origPosition;
   
    private float distanceToMove;
    //private float colliderDistance;
    
    void Start()
    {
        //this.colliderDistance = this.player.transform.position.y - this.cloudBackground.transform.position.y;
        //lastPlayerPosition = this.player.transform.position;
        lastBackgroundPosition = this.cloudBackground.transform.position;
        origPosition = gameObject.transform.position;
    }

    void Update()
    {
        //if (this.distanceToMove > 0)
        if (this.player.isOnLadder)
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + distanceToMove, this.transform.position.z);
        }
        //distanceToMove = this.player.transform.position.y - this.lastPlayerPosition.y;
        distanceToMove = this.cloudBackground.transform.position.y - this.lastBackgroundPosition.y;
        //lastPlayerPosition = this.player.transform.position;
        lastBackgroundPosition = this.cloudBackground.transform.position;
    }

    public void Reset()
    {
        this.distanceToMove = 0;
        lastBackgroundPosition = this.cloudBackground.transform.position;
        gameObject.transform.position = origPosition;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("[LoseCollider] Trigger detected");
        //levelManager.LoadLevel("game over");
        if (collider.gameObject.tag == "Player")
        {
            gameManager.RestartGame();
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("[LoseCollider] Collision detected");
    }
}