﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooler : MonoBehaviour
{
    public GameObject pooledObject;
    public GameObject[] pooledObjects;

    public int poolCapacity; // maximum number of instance of pooledObject
    public List<GameObject> objectList;

    private int objectIndex;

    // Use this for initialization
    void Start()
    {
        Debug.Log("[ObjectPooler] pool capacity: " + this.poolCapacity);
        objectList = new List<GameObject>();
        for (int i = 0; i < poolCapacity; ++i)
        {
            this.spawnNewObject();
        }
    }

    public GameObject getPoolObject()
    {
        for (int i = 0; i < this.objectList.Count; ++i)
        {
            // return non-active GameObject
            if (!this.objectList[i].activeInHierarchy)
            {
                return objectList[i];
            }

        }
        // if we don't find deactivated object, create new 
        this.spawnNewObject();
        return objectList[objectList.Count - 1];

    }

    public void Reset()
    {
        foreach (var obj in this.objectList) {
            obj.SetActive(false);
        }
    }

    // create new pooledObject and add it to the list
    private void spawnNewObject()
    {
        this.objectIndex = Random.Range(0, pooledObjects.Length);
        GameObject newObject = (GameObject)Instantiate(pooledObjects[objectIndex]);
        newObject.SetActive(false);
        objectList.Add(newObject);
    }

}
