﻿using UnityEngine;
using System.Collections;

public class PickupController : MonoBehaviour
{
    public int scoreAmount; // how much score 1 coin gives
    private ScoreManager scoreManager;

    // Use this for initialization
    void Start()
    {
        scoreManager = FindObjectOfType<ScoreManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("[PickUpController]: -------------------------------------- OnTriggerEnter2D");
        if (collider.gameObject.name == "Player")
        {
            Debug.Log("[PickUpController]: Player collision detected, increasing score");
            scoreManager.UpdateScore(this.scoreAmount);
            this.gameObject.SetActive(false);
        }
    }
}