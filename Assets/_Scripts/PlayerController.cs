﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    // variables controlling state of player
    public bool isOnLadder = false;// can only jump once it lands on a ladder (unable to double jump)
    public float climbSpeed;
    public float jumpForceX;
    public float jumpForceY;

    public float maxJumpTime;
    private float jumpCounter;

    private float behindLadderZ;
    private float infrontLadderZ;
    private bool isInFront;
    private bool isGameOver;

    // attached components
    private Animator myAnimator;
    private Collider2D myCollider;
    private Rigidbody2D myRigidbody;
    private Vector3 origPosition;

    // other game objects
    public Background background;
    public LayerMask laddersLayer;
    public LayerMask frontBlocksLayer;
    public LayerMask backBlocksLayer;
    public GameObject ladders; // "folder" holding all ladder objects

    void Start()
    {
        // GetComponent --> search object that script is attached to for given type
        myAnimator = GetComponent<Animator>();
        myCollider = GetComponent<Collider2D>();
        myRigidbody = GetComponent<Rigidbody2D>();
        isInFront = true;
        isGameOver = false;
        origPosition = this.gameObject.transform.position;

        behindLadderZ = this.ladders.transform.position.z + 0.1f;
        infrontLadderZ = this.ladders.transform.position.z - 0.1f;

        jumpCounter = maxJumpTime;
    }

    void Update()
    {
        // if we are on the ladder, increase y position based on climbSpeed
        //Debug.Log("[PlayerController] Update() ---------jumpCounter: " + this.jumpCounter);

        // climb up if we are on ladder
        if (this.isOnLadder)
        {
            Vector3 oldPosition = this.transform.position;
            Vector3 newPosition = new Vector3(oldPosition.x, oldPosition.y + this.climbSpeed, oldPosition.z);
            this.transform.position = newPosition;
        }
        this.handlePlayerInput();
        this.updateAnimator();
    }

    public void Reset()
    {
        this.gameObject.transform.position = this.origPosition;
        this.isGameOver = false;
    }

    void handlePlayerInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (isOnLadder)
            {
                myRigidbody.velocity = new Vector2(-jumpForceX, jumpForceY);
            }
        }
        // continous jumping
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (jumpCounter > 0)
            {
                myRigidbody.velocity = new Vector2(-jumpForceX, jumpForceY);
                jumpCounter -= Time.deltaTime;
            }
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (isOnLadder)
            {
                myRigidbody.velocity = new Vector2(jumpForceX, jumpForceY);
            }
        }
        // continuous jumping
        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (jumpCounter > 0)
            {
                myRigidbody.velocity = new Vector2(jumpForceX, jumpForceY);
                jumpCounter -= Time.deltaTime;
            }
        }
        // reset continous jumping
        if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            jumpCounter = 0;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (this.isInFront)
            {
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.behindLadderZ);
            }
            else
            {
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.infrontLadderZ);
            }
            this.isInFront = !this.isInFront;
        }
    }

    void updateAnimator()
    {
        myAnimator.SetBool("isOnLadder", this.isOnLadder);
        myAnimator.SetBool("isFacingBack", this.isInFront);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("[PlayerController] OnTriggerEnter2D() ------------------- Trigger ENTER detected");
        Debug.Log("[PlayerController] OnTriggerEnter2D() Collider: " + collider);

        if (this.isGameOver == true) { return; }

        if (Physics2D.IsTouchingLayers(myCollider, laddersLayer))
        {
            Debug.Log("[PlayerController] OnTriggerEnter2D() --------------- collider is ladder");
            this.OnPlayerEnterLadder(collider);
        }
        if (Physics2D.IsTouchingLayers(myCollider, frontBlocksLayer))
        {
            Debug.Log("[PlayerController] OnTriggerEnter2D() ---------- collider is front block");
            if (this.isInFront)
            {
                this.OnPlayerHitBlock();
                return;
            }
        }
        if (Physics2D.IsTouchingLayers(myCollider, backBlocksLayer))
        {
            Debug.Log("[PlayerController] OnTriggerEnter2D() ----------- collider is back block");
            if (!this.isInFront)
            {
                this.OnPlayerHitBlock();
                return;
            }
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        Debug.Log("[PlayerController] OnTriggerExit2D() --------------------- Trigger EXIT detected");
        Debug.Log("[OnTriggerExit2D] Collider: " + collider);

        if (this.isGameOver == true) { return; }

        if (!Physics2D.IsTouchingLayers(myCollider, laddersLayer))
        {
            Debug.Log("[PlayerController] OnTriggerExit2D() ---------------- collider is ladder");
            this.OnPlayerExitLadder();
        }
        if (Physics2D.IsTouchingLayers(myCollider, frontBlocksLayer))
        {
            Debug.Log("[PlayerController] OnTriggerEnter2D() ---------- collider is front block");
            if (this.isInFront)
            {
                this.OnPlayerHitBlock();
                return;
            }
        }
        if (Physics2D.IsTouchingLayers(myCollider, backBlocksLayer))
        {
            Debug.Log("[PlayerController] OnTriggerEnter2D() ----------- collider is back block");
            if (!this.isInFront)
            {
                this.OnPlayerHitBlock();
                return;
            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("[PlayerController] OnCollisionEnter2D() ------------- Trigger COLLISION detected");
    }

    private void OnPlayerHitBlock()
    {
        this.setGameOver();
    }

    private void setGameOver() {
        this.isGameOver = true;
        this.isOnLadder = false;
        this.myRigidbody.gravityScale = 1;
    }

    private void OnPlayerEnterLadder(Collider2D ladder)
    {
        Debug.Log("[PlayerController] OnPlayerEnterLadder() ---------------- Player ENTERING ladder");

        // stop jumping 
        myRigidbody.velocity = new Vector2(0, 0);

        // reset jump counter
        jumpCounter = maxJumpTime;

        // set horizontal position to ladder's
        this.transform.position = new Vector3(ladder.transform.position.x, this.transform.position.y, this.transform.position.z);

        this.myRigidbody.gravityScale = 0;
        this.isOnLadder = true;
        background.isPlayerOnLadder = true;
    }

    private void OnPlayerExitLadder()
    {
        Debug.Log("[OnPlayerExitLadder] ------------------------------------- Player EXITING ladder");

        this.myRigidbody.gravityScale = 1;
        this.isOnLadder = false;
        background.isPlayerOnLadder = false;

        if (this.isPlayerJumping() == false) {
            this.setGameOver();
        }
    }

    private bool isPlayerJumping()
    { // if we have no horzontal velocity, we are not jumping. Verticle velocity is affected by gravity
        return (myRigidbody.velocity.x == 0 ? false : true); 
    }
}