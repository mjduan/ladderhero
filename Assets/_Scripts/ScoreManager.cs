﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text scoreText;
    public Text highscoreText;

    public float score;
    public float highscore;
    public float pointsPerSec;

    public bool isScoreIncreasing;

    // Use this for initialization
    void Start()
    {
        if (PlayerPrefs.HasKey("HighScore"))
        {
            highscore = PlayerPrefs.GetFloat("HighScore", highscore);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isScoreIncreasing)
        {
            score += pointsPerSec * Time.deltaTime;

            if (score > highscore)
            {
                highscore = score;
                PlayerPrefs.SetFloat("HighScore", highscore);
            }
        }

        scoreText.text = "Score: " + Mathf.Round(score);
        highscoreText.text = "Highscore: " + Mathf.Round(highscore);
    }

    public void Reset()
    {
        score = 0;
        isScoreIncreasing = true;
    }
    
    public void UpdateScore (int scoreAmount)
    {
        Debug.Log("[ScoreManager]: UpdateScore() by: " + scoreAmount);
        this.score += scoreAmount;
    }
}
