﻿using UnityEngine;
using System.Collections;

public class StageController : MonoBehaviour
{
    public PlayerController thePlayer;
    public Background theBackground;

    public float playerSpeedIncrement; 
    public float stageDistance; // distance traveled by player in order to increment the stage

    private float distanceLimit;
    private float distanceTraveled;

    // Use this for initialization
    void Start()
    {
        distanceTraveled = thePlayer.transform.position.y;
        distanceLimit = distanceTraveled + stageDistance;
    }

    // Update is called once per frame
    void Update()
    {
        distanceTraveled = thePlayer.transform.position.y;
        //Debug.Log("[StageController] distanceTraveled: " + distanceTraveled );
        //Debug.Log("[StageController] distanceLimit: " + distanceLimit);

        if (distanceTraveled > distanceLimit)
        {
            //this.NextStage();
        }
    }

    void NextStage()
    {
        //Debug.Log("[StageController] NextStage() --------- incrementing stage" );
        distanceLimit += stageDistance;
        //stageDistance *= 1.25;
        thePlayer.climbSpeed += playerSpeedIncrement;
    }
}